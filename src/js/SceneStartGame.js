import { GameState } from './GameState.js'

export let SceneStartGame = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function () {
        Phaser.Scene.call(this, { "key": "SceneStartGame" })
            let resize = () => {
            if (this.sys.game.scene.isActive(this.scene.key)) {
                this.scene.restart()
            }
        }

        window.addEventListener("resize", resize.bind(this), false);
    },
    init: function (gameSettings) {
        this.gameSettings = gameSettings
    },
    preload: function () {
        this.load.html('newGameForm', 'text/newGameForm.html')
    },
    create: function () {

        const element = this.add.dom(this.sys.game.canvas.width / 2, this.sys.game.canvas.height / 2).createFromCache('newGameForm')
        document.getElementById('playerName').focus()
        element.addListener('click')
        let scene = this
        element.on('click', function (event) {
            if (event.target.name === 'playerName') {
                playerName.classList.remove('input-error')
            }
            if (event.target.name === 'startButton') {
                let playerName = this.getChildByName('playerName')

                if (playerName.value.trim() !== '') {
                    this.removeListener('click')
                    let gameState = new GameState(playerName.value)
                    scene.cameras.main.fadeOut(1000, 0, 0, 0)
                    scene.scene.start("SceneWorldOne", { gameState: gameState, gameSettings: scene.gameSettings })
                } else {
                    if (playerName.value.trim() === '') {
                        playerName.classList.add('input-error')
                    }
                }
            }
        })

        this.cameras.main.fadeIn(1000, 0, 0, 0)
    },
    update: () => { }
})