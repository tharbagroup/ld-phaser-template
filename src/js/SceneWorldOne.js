import { GameSettings } from './GameSettings.js'

export let SceneWorldOne = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function () {
        console.log('init called')
        Phaser.Scene.call(this, { "key": "SceneWorldOne" })
                let resize = () => {
            if (this.sys.game.scene.isActive(this.scene.key)) {
                console.log('restart')
                this.scene.restart()
            }
        }

        window.addEventListener("resize", resize.bind(this), false);
    },
    init: function (gameSettings = null) {
        if (!(gameSettings instanceof GameSettings)) {
            this.gameSettings = new GameSettings()
        } else {
            this.gameSettings = gameSettings
        }

    },
    preload: () => { },
    create: function () {
        console.log('create called')
        let menuText = this.add.text(
            this.sys.game.canvas.width / 2,
            this.sys.game.canvas.height / 3,
            "Ludum Dare 50",
            {
                fontSize:64,
                color: "#55FF55",
                fontStyle: "bold"
            }
        ).setOrigin(0.5)

        let startButton = this.add.text(
            this.sys.game.canvas.width / 2 - 200,
            this.sys.game.canvas.height / 1.5,
            "Start",
            {
                fontSize: 32,
                color: "#FFFFFF",
                fontStyle: "bold",
                backgroundColor: "#8888ff",
                padding: { x: 10, y: 10 },

            }
        )
            .setOrigin(0.5)
            .setInteractive({ useHandCursor: true })
            .on('pointerdown', () => {
                this.scene.start("SceneStartGame", this.gameSettings)
            })

        let settingsButton = this.add.text(
            this.sys.game.canvas.width / 2 + 200,
            this.sys.game.canvas.height / 1.5,
            "Settings",
            {
                fontSize: 32,
                color: "#FFFFFF",
                fontStyle: "bold",
                backgroundColor: "#ff8888",
                padding: { x: 10, y: 10 },

            }
        )
            .setOrigin(0.5)
            .setInteractive({ useHandCursor: true })
            .on('pointerdown', () => { this.scene.start("SceneSetting", this.gameSettings) })

        this.cameras.main.fadeIn(1000, 0, 0, 0)
        console.log('create finished')

    },
    startGame: function () {
    },
    update: function () {
        let cursors = this.input.keyboard.createCursorKeys()
        if (cursors.space.isDown) {
            this.scene.start("SceneGameOne")
        }
    }
})