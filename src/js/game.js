import { SceneStartGame } from './SceneStartGame.js'
import { SceneGameSettings } from './SceneGameSettings.js' 
import { SceneWorldOne } from './SceneWorldOne.js'

const phaserSettings = {
    type: Phaser.AUTO,
    parent: "game",
    width: window.innerWidth,
    height: window.innerHeight,
    backgroundColor: "#000000",
    scene: [ SceneWorldOne, SceneStartGame, SceneGameSettings ],
    dom: {
        createContainer: true
    },
    pixelArt: true
}

const game = new Phaser.Game(phaserSettings)

window.addEventListener('resize', () => {
    game.scale.resize(window.innerWidth, window.innerHeight)
})
